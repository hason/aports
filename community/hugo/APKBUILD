# Contributor: Thomas Boerger <thomas@webhippie.de>
# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Thomas Boerger <thomas@webhippie.de>
pkgname=hugo
pkgver=0.84.1
pkgrel=0
pkgdesc="Fast and flexible static site generator written in Go"
url="https://gohugo.io/"
arch="all"
license="Apache-2.0"
makedepends="go"
subpackages="$pkgname-doc $pkgname-bash-completion"
source="https://github.com/gohugoio/hugo/archive/v$pkgver/hugo-$pkgver.tar.gz
	skip-git-tests.patch
	skip-para-test.patch
	skip-js-tests.patch
	increase-timeout-pagebundler-test.patch
	"

# npm is not available on mips64
if [ "$CARCH" != "mips64" ]; then
	checkdepends="npm"
fi

build() {
	go build -v -o bin/$pkgname --tags extended
	./bin/hugo gen man
	./bin/hugo gen autocomplete --completionfile="$builddir"/hugo.bash
}

check() {
	go test ./...
}

package() {
	install -Dm755 bin/$pkgname "$pkgdir"/usr/bin/$pkgname
	install -Dm644 man/*.1 -t "$pkgdir"/usr/share/man/man1
	install -Dm644 hugo.bash \
		"$pkgdir"/usr/share/bash-completion/completions/hugo
}

sha512sums="
2f0268eee308b68c8b96ec2a72875a2e76393f8b0216306b6378e083254926925ed869a5e6a3d6c7489f1bc0a272ec87948156c7cc8a65e99d7bcecd5edb619c  hugo-0.84.1.tar.gz
f07d458b5df72d0b10e0fd9ec6120502b62af1ee347f0ec83eda99147c525327c0098150db80450da48c5bc03d825279e35f0940dae5d909203757abb6bd1a20  skip-git-tests.patch
6ba192d8cb67f115f7ce596c297a55fc64713a4cdb0077cfbb7e45051c7560f5b668da88f513d4f34d8e0eeb4a9d991c5312d62e454c85e95960d8a33f0f8f69  skip-para-test.patch
0eada6d4f318775d24a55bec65349e62148b71322f084874a608596f8248cc948632e5f1823c3e86463090e35777e14784c2bcdb6493ad71be9b98ecc4700f24  skip-js-tests.patch
0eafc165979d869ef401fec050678ba025431ac66b79a9761879024da8326e2dd5e9d14d90e32c3071d83dbac6973c2b4f954217e789944524b660a09131b298  increase-timeout-pagebundler-test.patch
"
